# import io
import json
import os
import re
import ssl
import sys
import time
import urllib.error
import urllib.request
from datetime import timedelta
from glob import glob

import werkzeug
from flask import (
    Flask,
    Response,
    flash,
    redirect,
    render_template,
    request,
    send_from_directory,
    session,
    url_for,
)

from .SNPWorkFlowMessage import SNPWorkFlowMessage

sys.path.append("../")
from setup_genome_list import setup_genome_list

ssl._create_default_https_context = ssl._create_unverified_context
import subprocess

PWD = os.getcwd()
GENOMES = "/home/genomes"
DATA = "/home/app/SNPWorkFlow/data"
INPUT = os.path.join(DATA, "input")
OUTPUT = os.path.join(DATA, "output")
ROOT = "https://plantgarden.jp"
RESEQ_PBP = "/home/app/reseq_pbp"
select_genome = None
read_number = 1
GENOME_LIST_PATH = "/home/app/SNPWorkFlow/genome_list.json"
DETAIL_MESSAGES = SNPWorkFlowMessage.SNP_HELP_MESSAGES

LIST_GROUP_ITEM_DANGER = "list-group-item-danger"
LIST_GROUP_ITEM_SUCCESS = "list-group-item-success"

SELECT_GENOME = "select_genome"

MAKECONFIG_LOG = "/home/app/reseq_pbp/makeconfig_log.txt"
MAIN_LOG = "/home/app/reseq_pbp/outdir/log.txt"

# Flaskオブジェクトの生成
app = Flask(__name__)

app.secret_key = "abcdefghijklmn"
app.permanent_session_lifetime = timedelta(minutes=60)


@app.route("/favicon.ico")
def favicon():
    return app.send_static_file("favicon.ico")


@app.route("/", methods=["GET"])
def route():
    """[summary]
    「/」へアクセスがあった場合に、「select-genome.html」を返す
    Args:

    Returns:
        Response
    """
    env_init()
    return render_template("index.html")


@app.route("/select-genome", methods=["GET"])
def select_genome():
    """[summary]
    「/select-genome」へアクセスがあった場合に、「select-genome.html」を返す
    Args:

    Returns:
        Response
    """
    genome_items = make_genome_items()

    return render_template(
        "select-genome.html",
        detail_messages=DETAIL_MESSAGES,
        reference_genomes=genome_items,
    )


@app.route("/help", methods=["GET"])
def help():
    """[summary]
    「/help」へアクセスがあった場合に、「help.html」を返す
    Args:

    Returns:
        Response
    """
    if request.method == "GET":
        return render_template(
            "help.html",
            title="help",
        )


@app.route("/update-genome-list", methods=["POST"])
def update_genome_list():

    is_update = request.form.getlist("check")

    try:
        if "is_update" in is_update:
            setup_genome_list()
            flash("success: update genome list", LIST_GROUP_ITEM_SUCCESS)
    except Exception as e:
        flash(e, LIST_GROUP_ITEM_DANGER)

    return redirect(url_for(SELECT_GENOME))


@app.route("/post1", methods=["POST"])
def post1():
    """[summary]
    対象ゲノムファイルのダウンロード、解凍、マッピングを行い、
    エラーメッセージがあれば、フラッシュメッセージ付きの「select-genome.html」を
    なければ、「/settings.html」を返す
    Args:

    Returns:
        Response | error: Responseもしくはエラーメッセージ
    """
    session.permanent = True
    reference = json.loads(request.form.get("select_genome"))

    dir_name = reference["name"]
    rURL = reference["path"]
    file_name = reference["genome_name"]
    reference_path = os.path.join(GENOMES, dir_name)
    reference_file_path = os.path.join(reference_path, file_name)

    session["name"] = dir_name
    session["genome_name"] = file_name

    # ユーザーカスタムゲノムでない場合かつ、ファイルが存在しない場合のみ、ダウンロード
    # ユーザーカスタムゲノムはrURLがNone
    if rURL is not None and not os.path.isfile(reference_file_path):
        if not os.path.isdir(reference_path):
            os.makedirs(reference_path)
        # 対象ゲノムファイルのダウンロード
        error_text = download_file(rURL, reference_file_path)
        if error_text:
            flash(error_text)
            return redirect(url_for("select_genome"))

    return redirect(url_for("settings"))


@app.route("/post2", methods=["POST"])
def post2():
    """[summary]
    対象ゲノムファイルのダウンロード、解凍、マッピングを行い、
    エラーメッセージがあれば、フラッシュメッセージ付きの「select-genome.html」を
    なければ、「/settings.html」を返す
    Args:

    Returns:
        Response | error: Responseもしくはエラーメッセージ
    """
    session.permanent = True

    dir_name = request.form.get("send_file")
    reference_path = os.path.join(GENOMES, dir_name)

    # リファレンスゲノムgzファイルのアップロード
    reference_extension = ["gz"]
    res = uploadFlow(reference_path, "reference_file", reference_extension)
    if res == "extension_error":
        flash(
            f"{SNPWorkFlowMessage.SNP_ERROR_0008}: reference_genome_file:{reference_extension}",
            LIST_GROUP_ITEM_DANGER,
        )
        return redirect(url_for("select_genome"))

    file_name = res[0]
    # reference_path = os.path.join(GENOMES, dir_name)

    session["name"] = dir_name
    session["genome_name"] = file_name

    return redirect(url_for("settings"))


@app.route("/settings", methods=["GET"])
def settings():
    """[summary]
    「/settings」へアクセスがあった場合に、「settings.html」を返す
    Args:

    Returns:
        Response
    """
    return render_template(
        "settings.html",
        title="Settings",
        select_genome=session["name"],
        detail_messages=DETAIL_MESSAGES,
    )


@app.route("/analysis", methods=["POST"])
def analysis():
    """[summary]
    選択されたリファレンスゲノムとリードタイプをセッションに記録して、
    「analysis.html」を返す
    Args:

    Returns:
        Response
    """
    # セッション切れに対する対応
    try:
        name = session["name"]
        genome_name = session["genome_name"]
    except KeyError:
        flash(SNPWorkFlowMessage.SNP_ERROR_0009, LIST_GROUP_ITEM_DANGER)
        return redirect(url_for("select_genome"))

    make_log(MAIN_LOG)
    make_log(MAKECONFIG_LOG)

    fastqc = request.form["q_check"]

    adapter_trim = request.form["adapter_trim"]
    if adapter_trim == "T":
        adapter5 = (
            "None"
            if (request.form["adapter5"] is None or request.form["adapter5"] == "")
            else request.form["adapter5"]
        )
        adapter3 = (
            "None"
            if (request.form["adapter3"] is None or request.form["adapter3"] == "")
            else request.form["adapter3"]
        )
        if adapter5 == "None" and adapter3 == "None":
            flash(SNPWorkFlowMessage.SNP_ERROR_0001, LIST_GROUP_ITEM_DANGER)
            return redirect(url_for("settings"))
    else:
        adapter5 = "None"
        adapter3 = "None"

    qc = request.form["q_control"]
    if qc == "T":
        qc_val = (
            "30"
            if request.form["q_control_val"] is None
            or request.form["q_control_val"] == ""
            else request.form["q_control_val"]
        )
    else:
        qc_val = "None"

    mapping = request.form["mapping"]

    vc = request.form["variant_call"]
    if vc == "T":
        if mapping == "F":
            flash(SNPWorkFlowMessage.SNP_ERROR_0002, LIST_GROUP_ITEM_DANGER)
            return redirect(url_for("settings"))

        vcf = request.form["vcf"]
        gvcf = request.form["gvcf"]

        if vcf == "F" and gvcf == "F":
            flash(SNPWorkFlowMessage.SNP_ERROR_0003, LIST_GROUP_ITEM_DANGER)
            return redirect(url_for("settings"))

    else:
        vcf = "F"
        gvcf = "F"

    # sample.iniのアップロード
    sample_ini_extension = ["ini"]
    res = uploadFlow(RESEQ_PBP, "sample_ini", sample_ini_extension)
    if res == "error":
        flash(SNPWorkFlowMessage.SNP_ERROR_0004, LIST_GROUP_ITEM_DANGER)
        return redirect(url_for("settings"))
    elif res == "extension_error":
        flash(
            f"{SNPWorkFlowMessage.SNP_ERROR_0008}: sample_ini:{sample_ini_extension}",
            LIST_GROUP_ITEM_DANGER,
        )
        return redirect(url_for("settings"))
    ini_file_name = res[0]

    # fastqFileのアップロード
    res = uploadFlow(INPUT, "fastqFile[]")
    if res == "error":
        flash(SNPWorkFlowMessage.SNP_ERROR_0005, LIST_GROUP_ITEM_DANGER)
        return redirect(url_for("settings"))

    cmd = (
        f"/bin/bash /home/app/reseq_pbp/reseq_pbp.sh "
        f"{fastqc} {qc} {qc_val} {adapter_trim} "
        f"{adapter3} {adapter5} {mapping} {vc} {vcf} {gvcf} "
        f"{name} {genome_name} {ini_file_name}"
    )

    subprocess.run(cmd, shell=True)
    downloadFilePath = glob(f"{OUTPUT}/outdir-{name}*.zip")[0]
    session["downloadFilePath"] = downloadFilePath

    return redirect(url_for("result"))


@app.route("/result", methods=["GET", "POST"])
def result():
    flash(SNPWorkFlowMessage.SNP_MESSAGE_001, LIST_GROUP_ITEM_SUCCESS)
    return render_template("result.html")


@app.route("/download", methods=["GET"])
def download():
    try:
        downloadFile = session["downloadFilePath"].split("/")[-1]
        return send_from_directory(
            OUTPUT, downloadFile, mimetype="zip", as_attachment=True
        )
    except Exception:
        return f'<h1>{SNPWorkFlowMessage.SNP_ERROR_0006}</h1> <a href="select-genome">back</a>'


@app.route("/makeconfig-log", methods=["GET"])
def makeconfig_log():
    """returns logging information"""
    return Response(
        exome_log(MAKECONFIG_LOG),
        mimetype="text/plain",
        content_type="text/event-stream",
    )


@app.route("/reseq-pbp-log", methods=["GET"])
def reseq_pbp_log():
    """returns logging information"""
    return Response(
        exome_log(MAIN_LOG), mimetype="text/plain", content_type="text/event-stream"
    )


@app.route("/log", methods=["GET"])
def log():
    return render_template("log.html")


def make_log(path):
    """[summary]
    ログファイルを作成
    Args:
        path(str): 作成するログファイルのパス
    Returns:

    """
    path_list = path.split("/")
    path_list.pop(-1)
    dir_path = "/".join(path_list)

    cmd = f"mkdir -p {dir_path} && touch {path}"
    subprocess.run(cmd, shell=True)


def download_file(url, dst_path):
    """[summary]
    ファイルのダウンロード処理（詳細）
    Args:
        url(str): ダウンロードURL
        dst_path(str): ダウンロードファイルの保存パス
    Returns:
        Error Message(str) | None
    """
    try:
        with urllib.request.urlopen(url) as web_file:
            data = web_file.read()
            with open(dst_path, mode="wb") as local_file:
                local_file.write(data)
        return None
    except urllib.error.URLError as e:
        return f"{SNPWorkFlowMessage.SNP_ERROR_0007}: {e}"


def uploadFlow(save_dir, file_type, allowed_extensions=None):
    """[summary]
    ファイルのアップロード処理（詳細）
    Args:
        save_dir(str): アップロード先のディレクトリ
        file_type(str): HTML側のinputフォームのネームダグ名
        allowed_extensions(list | None): 許容する拡張子のリスト(".tar.gz"のように2つ以上用いるものに対しては未対応)
    Returns:
        file_names(list)
    """
    files = request.files.getlist(file_type)
    file_names = []

    for file in files:
        file_name = file.filename

        # 空文字チェック
        if file_name == "":
            return "error"

        # 拡張子チェック
        if allowed_extensions is not None:
            extension_type = file_name.split(".")[-1]
            if extension_type not in allowed_extensions:
                return "extension_error"

        if not os.path.isdir(save_dir):
            os.makedirs(save_dir)

        save_file_name = werkzeug.utils.secure_filename(file_name)
        file_names.append(file_name)
        file.save(os.path.join(save_dir, save_file_name))

    return file_names


def exome_log(log_path):
    """[summary]
    ログを出力
    Args:

    Returns:

    """
    try:
        with open(log_path, "r") as f:
            while True:
                content = f.read()
                yield content.encode()
                time.sleep(5)
        # return Response(content)
    except FileNotFoundError:
        yield "No log \n"
        time.sleep(10)
        exome_log(log_path)


if __name__ == "__main__":
    app.run(debug=True)


def env_init():
    """[summary]
    解析の過程で作成されたファイル、セッションを削除する
    Args:

    Returns:

    """
    cmd = f"rm -rf {INPUT}/*"
    subprocess.Popen(cmd, shell=True)
    cmd = f"rm -rf {OUTPUT}/*"
    subprocess.Popen(cmd, shell=True)
    session.clear()


def make_genome_items():
    """[summary]
    ゲノムリストの作成（詳細）
    Args:

    Returns:
        genome_items(list)
    """

    fasta_extensions = [".fasta", ".fna", ".fa"]
    fasta_gz_extensions = ["fasta.gz", ".fna.gz", ".fa.gz"]

    fasta_gz_reg_exp = "$|".join(fasta_gz_extensions).replace(".", r"\.") + "$"
    fasta_reg_exp = "$|".join(fasta_extensions).replace(".", r"\.") + "$"

    # 取得済みのゲノムフォルダーを取得
    local_gemone_info = []
    for dir_name in os.listdir(GENOMES):
        if not os.path.isdir(os.path.join(GENOMES, dir_name)):
            continue
        is_fasta = False
        genome_info = {}

        for file in os.listdir(os.path.join(GENOMES, dir_name)):
            # fasta_file存在チェック(gz)
            if not is_fasta and re.search(fasta_gz_reg_exp, file):
                is_fasta = True
                genome_info["genome"] = file
            # fasta.gzが存在している場合、リストに追加
            if is_fasta:
                genome_info["dir_name"] = dir_name
                local_gemone_info.append(genome_info)
                break

        if not (is_fasta):
            for file in os.listdir(os.path.join(GENOMES, dir_name)):
                # fasta_file存在チェック
                if not is_fasta and re.search(fasta_reg_exp, file):
                    is_fasta = True
                    genome_info["genome"] = file
                # fastaが存在している場合、リストに追加
                if is_fasta:
                    genome_info["dir_name"] = dir_name
                    local_gemone_info.append(genome_info)
                    break

    # 標準ゲノムリストの作成
    genome_items = []
    with open(GENOME_LIST_PATH) as f:
        for genome in json.load(f):
            name = f"{genome['Folder_name']}_{genome['genome_assembly_id']}"  # dir_name

            if genome.get("genome") is None:
                continue


            genome_name = genome["genome"].split("/")[-1]

            # 取得済みのゲノムについてはゲノム名に"取得済み"と追記
            _name = name
            genome_info = {"dir_name": name, "genome": genome_name}
            if genome_info in local_gemone_info:
                local_gemone_info.remove(genome_info)
                _name = "".join([name, "(取得済み)"])

            genome_items.append(
                {
                    "name": _name,
                    "item": json.dumps(
                        {
                            "name": name,
                            "path": genome["genome"],
                            "genome_name": genome_name,
                        }
                    ),
                }
            )

    # ユーザーカスタム用のゲノムリストを追加
    for gemone_info in local_gemone_info:
        _name = "".join([gemone_info["dir_name"], "(ユーザーカスタム)"])

        genome_items.append(
            {
                "name": _name,
                "item": json.dumps(
                    {
                        "name": gemone_info["dir_name"],
                        "path": None,
                        "genome_name": gemone_info["genome"],
                    }
                ),
            }
        )

    return genome_items
