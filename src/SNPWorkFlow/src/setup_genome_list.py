import json
import re

import requests
import urllib3
from urllib3.exceptions import InsecureRequestWarning

urllib3.disable_warnings(InsecureRequestWarning)
ROOT = "https://plantgarden.jp"
TAR_GZ_EXTENSIONS = [".tar", ".tar.gz"]


def setup_genome_list():
    """[summary]
    外部APIを元に新たなゲノム一覧を作成する
    """
    genome_list_url = "https://plantgarden.jp/api/1/getGenomeIdList"

    genome_json = requests.get(genome_list_url, verify=False).json()

    genome_list = []
    tar_gz_reg_exp = "$|".join(TAR_GZ_EXTENSIONS).replace(".", r"\.") + "$"
    for k, v in genome_json.items():
        if v["file_path"]["genome"] is None or v["file_path"]["gff"] is None:
            continue

        if re.search(tar_gz_reg_exp, v["file_path"]["genome"]) or re.search(
            tar_gz_reg_exp, v["file_path"]["gff"]
        ):
            continue

        genome_list.append(
            {
                "genome": f'{ROOT}{v["file_path"]["genome"]}',
                "gff": f'{ROOT}{v["file_path"]["gff"]}',
                "Folder_name": v["species_name"]
                .replace(" ", "_")
                .replace("(", "")
                .replace(")", ""),
                "genome_assembly_id": k,
                "species_id_taxonomy_id": v["species_id"],
            }
        )

    with open("/home/app/SNPWorkFlow/genome_list.json", "w") as writer:
        # writer.write(genome_list)
        json.dump(genome_list, writer, indent=2)


setup_genome_list()
