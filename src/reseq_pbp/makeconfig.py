import argparse
import configparser
import logging
import os
import subprocess
import sys
from subprocess import PIPE


class MakeConfig:
    def __init__(self):
        self.argument_parser()
        self.setup_logger()

    def argument_parser(self):
        parser = argparse.ArgumentParser(description="reseq config maker")
        parser.add_argument(
            "-t",
            "--tmp_conf",
            dest="tmp",
            type=str,
            help="template config file to get path to tools",
            required=False,
            default=os.path.join(os.path.dirname(__file__), "tmp.conf"),
        )
        parser.add_argument(
            "-oc",
            "--output_conf",
            dest="oc",
            type=str,
            help="output config file",
            required=False,
            default="out.conf",
        )
        parser.add_argument(
            "-g",
            "--genome",
            dest="genome",
            type=str,
            help="path to genome fasta",
            required=True,
            default=None,
        )
        parser.add_argument(
            "-init",
            "--initialization",
            dest="init",
            action="store_true",
            help="check and make index of genome for reseq execution",
            required=False,
        )
        parser.add_argument(
            "-outdir",
            "--output_dir",
            dest="outdir",
            type=str,
            help="directory to output results",
            required=True,
            default=None,
        )
        parser.add_argument(
            "-indir",
            "--input_dir",
            dest="indir",
            type=str,
            help="directory which contains input fastqs",
            required=True,
            default=None,
        )
        parser.add_argument(
            "-fastqc",
            "--fastqc",
            dest="fastqc",
            type=str,
            choices=["T", "F"],
            default="F",
            help="choose from T(rue) and F(alse)",
            required=False,
        )
        parser.add_argument(
            "-at",
            "--adapter_trim",
            dest="at",
            type=str,
            choices=["T", "F"],
            default="F",
            help="choose from T(rue) and F(alse)",
            required=False,
        )
        parser.add_argument(
            "-qc",
            "--quality_control",
            dest="qc",
            type=str,
            choices=["T", "F"],
            default="F",
            help="choose from T(rue) and F(alse)",
            required=False,
        )
        parser.add_argument(
            "-map",
            "--mapping",
            dest="map",
            type=str,
            choices=["T", "F"],
            default="F",
            help="choose from T(rue) and F(alse)",
            required=False,
        )
        parser.add_argument(
            "-vc",
            "--variant_call",
            dest="vc",
            type=str,
            choices=["T", "F"],
            default="F",
            help="choose from T(rue) and F(alse)",
            required=False,
        )
        parser.add_argument(
            "-vcf",
            "--emit_vcf",
            dest="vcf",
            type=str,
            choices=["T", "F"],
            default="F",
            help="choose from T(rue) and F(alse)",
            required=False,
        )
        parser.add_argument(
            "-gvcf",
            "--emit_gvcf",
            dest="gvcf",
            type=str,
            choices=["T", "F"],
            default="F",
            help="choose from T(rue) and F(alse)",
            required=False,
        )
        parser.add_argument(
            "-a3p",
            "--adapter_3p",
            dest="a3p",
            type=str,
            default="",
            help="3' adapters : -a3p ATCGGA,AAAGGGCCC",
            required=False,
        )
        parser.add_argument(
            "-a5p",
            "--adapter_5p",
            dest="a5p",
            type=str,
            default="",
            help="5' adapters : -a5p ATCGGA,AAAGGGCCC",
            required=False,
        )
        parser.add_argument(
            "-aer",
            "--adapter_error_rate",
            dest="aer",
            type=str,
            default="0.05",
            help="adapter error rate : default 0.05",
            required=False,
        )
        parser.add_argument(
            "-q",
            "--quality",
            dest="q",
            type=int,
            default="30",
            help="base cutoff quality : default 30",
            required=False,
        )
        parser.add_argument(
            "-thre",
            "--threads",
            dest="thre",
            type=int,
            default=1,
            help="num of threads : default 1",
            required=False,
        )
        parser.add_argument(
            "-log",
            "--log",
            dest="log",
            type=str,
            default="makeconfig_log.txt",
            help="makeconfig logfile : default makeconfig_log.txt",
            required=False,
        )
        self.args = parser.parse_args()

    def setup_logger(self, name=None):
        logger = logging.getLogger(name)
        logger.setLevel(logging.DEBUG)
        # creates a file handler that logs messages above DEBUG level
        fh = logging.FileHandler(self.args.log)
        fh.setLevel(logging.DEBUG)
        fh_formatter = logging.Formatter(
            "[%(asctime)s] %(levelname)s %(filename)s %(funcName)s :\n%(message)s"
        )
        fh.setFormatter(fh_formatter)
        # creates a file handler that logs messages above INFO level
        sh = logging.StreamHandler()
        sh.setLevel(logging.INFO)
        sh_formatter = logging.Formatter(
            "[%(asctime)s] %(levelname)s %(filename)s : %(message)s",
            "%Y-%m-%d %H:%M:%S",
        )
        sh.setFormatter(sh_formatter)
        # add the handlers to logger
        logger.addHandler(fh)
        logger.addHandler(sh)
        self.logger = logger

    def execute_cmd(self, cmd, shell=True):
        self.logger.info("[cmd] {}".format(cmd))
        proc = subprocess.run(cmd, shell=shell, stdout=PIPE, stderr=PIPE, text=True)
        stdout = proc.stdout
        stderr = proc.stderr
        if len(stdout) > 1:
            self.logger.info(stdout)
        if len(stderr) > 1:
            self.logger.info(stderr)
        return stdout, stderr

    def parse_config(self):
        conf = configparser.ConfigParser()
        conf.optionxform = str
        conf.read(self.args.tmp, "UTF-8")
        self.tools = dict(conf.items("tools"))

    def check_input_info(self):
        # check tools
        for tool in self.tools.keys():
            ToF = os.path.exists(self.tools[tool])
            if ToF is False:
                self.logger.info(
                    "{} : no such a tool : {}".format(tool, self.tools[tool])
                )
                sys.exit(1)
        # check genome
        genome = self.args.genome
        ToF = os.path.isfile(genome)
        if ToF is False:
            self.logger.info("no such a genome file : {}".format(genome))
            sys.exit(1)

    def initialize(self):
        if self.args.init is not True:
            self.logger.info("skip checking or making index")
            return 0
        # index file check
        genome = self.args.genome
        # samtools
        samtools_index = os.path.isfile(genome + ".fai")
        # bwa
        for ind in [".amb", ".ann", ".bwt", ".sa", ".pac"]:
            bwa_index = os.path.isfile(genome + ind)
            if bwa_index is False:
                break
        # gatk
        if genome.endswith(".gz"):
            genome_name = ".".join(genome.strip().split(sep=".")[:-2])
        else:
            genome_name = ".".join(genome.strip().split(sep=".")[:-1])
        gatk_index = os.path.isfile(genome_name + ".dict")
        # prepare for indexing
        samtools = self.tools["samtools"]
        bwa = self.tools["bwa"]
        gatk = self.tools["gatk"]
        # index command
        cmd_samtools = samtools + " faidx {genome}"
        cmd_bwa = bwa + " index {genome}"
        cmd_gatk = gatk + " CreateSequenceDictionary -R {genome}"
        exec_dict = {
            "samtools": {"exists": samtools_index, "cmd": cmd_samtools},
            "bwa": {"exists": bwa_index, "cmd": cmd_bwa},
            "gatk": {"exists": gatk_index, "cmd": cmd_gatk},
        }
        # execute command
        for tool in exec_dict.keys():
            Exists = exec_dict[tool]["exists"]
            cmd = exec_dict[tool]["cmd"]
            if Exists:
                self.logger.info(
                    "Index for {} is already exists. Skip indexing".format(tool)
                )
            else:
                self.execute_cmd(cmd.format(genome=genome))

    def make_config(self):
        lines = []
        # make tools paths
        lines.append("[tools]")
        for tool in self.tools.keys():
            lines.append(tool + " = " + self.tools[tool])
        # make settings
        lines.append("[settings]")
        # append genome
        lines.append("genome_index = " + self.args.genome)
        # input _ output dir
        lines.append("input_dir = " + self.args.indir)
        lines.append("output_dir = " + self.args.outdir)
        # settings for pipeline
        lines.append("fastqc = " + self.args.fastqc)
        lines.append("adapter_trim = " + self.args.at)
        lines.append("quality_control = " + self.args.qc)
        lines.append("mapping = " + self.args.map)
        lines.append("variant_call = " + self.args.vc)
        if self.args.vc == "T":
            lines.append("markduplicates = T")
        lines.append("emit_vcf = " + self.args.vcf)
        lines.append("emit_gvcf = " + self.args.gvcf)
        # settings for each process
        lines.append("threads = " + str(self.args.thre))
        if self.args.qc == "T":
            lines.append("quality_cutoff = " + str(self.args.q))
        if self.args.at == "T":
            lines.append("adapters_3p = " + self.args.a3p)
            lines.append("adapters_5p = " + self.args.a5p)
            lines.append("adapter_error_rate = " + self.args.aer)
        # check analysis format
        # variant call without mapping
        if self.args.fastqc == "T" or self.args.qc == "T" or self.args.at == "T":
            if self.args.map == "F" and self.args.vc == "T":
                self.logger.info(
                    "you should choose mapping too to call variant form fastqs"
                )
                sys.exit(1)
        # adapter trim without adapters
        if self.args.at == "T":
            if self.args.a3p == "" and self.args.a5p == "":
                self.logger.info("you should input 3' or 5' adapter sequences")
        # output vcf and/or gvcf
        if self.args.vcf == "F" and self.args.gvcf == "F" and self.args.vc == "T":
            self.logger.info("please choose output format for variant call")
            sys.exit(1)
        with open(self.args.oc, "w") as oc:
            oc.write("# command : " + " ".join(sys.argv) + "\n")
            oc.write("\n".join(lines))

    def run(self):
        self.parse_config()
        self.check_input_info()
        self.initialize()
        self.make_config()


def main():
    MC = MakeConfig()
    MC.run()


if __name__ == "__main__":
    main()
