class SNPWorkFlowMessage:
    """
    メッセージ
    """

    # NOMAL
    SNP_MESSAGE_001 = "解析が完了しました。"

    # HELP
    SNP_HELP_MESSAGE_0001 = (
        "ゲノムを選択してください。初めて選んだゲノムについてはPlantGARDENからダウンロードが開始されますので、"
        "インターネットの接続をご確認ください。選択が二度目以降のものについてはダウンロード済みのデータを用いるので、"
        "ダウンロードは発生しません。"
    )
    SNP_HELP_MESSAGE_0002 = "リファレンスファイル選択で選んだゲノムが表示されます。"
    SNP_HELP_MESSAGE_0003_1 = (
        "[Choose sample.ini file]にはCSV形式で入力する検体名とファイルの対応を記載したsample.ini（ページ下部に入力ファイル例を記載）を入力してください。"
        "サンプル名が記載されたファイルについては、.iniという末尾でアップロードしてください。"
    )
    SNP_HELP_MESSAGE_0003_2 = "[Choose fastq files]にはsample.iniに記載した検体をブラウザなどから選択して入力してください（シフトを押しながら選択や範囲選択することで複数ファイルをアップロード可能です。）"

    SNP_HELP_MESSAGE_0004 = "解析実行前のFASTQファイルに対して、fastqcを用いてQuality checkを行います。"
    SNP_HELP_MESSAGE_0005 = (
        "cutadaptを用いてAdapter trimやQuality controlを行います。Adapter trimでは3', 5'それぞれについてadapterの入力が可能です"
        "（半角ローマ字の大文字で入力してください）。Quality controlについてもクオリティ値を指定して実行可能です（半角数字で入力してください）。"
    )
    SNP_HELP_MESSAGE_0006 = "Select genomeに表示されたゲノムに対してbwaを用いてリードのマッピングを行います。"
    SNP_HELP_MESSAGE_0007 = (
        "gatkを用いてvariant callを行います。出力をgVCF / VCFの片方または両方で指定できます。"
        "また、本工程は必ず[Mapping on a reference]とセットで行ってください。"
    )
    SNP_HELP_MESSAGE_0008 = "RUNでこのページで選んだ設定で解析を実行します。解析の実行は別タブが開いて解析のプロセスが表示されていきます。解析が終了するまでは実行中のタブを消さないでください。"

    SNP_HELP_MESSAGES = {
        "select_genome0": SNP_HELP_MESSAGE_0001,
        "select_genome": SNP_HELP_MESSAGE_0002,
        "choose_file": (SNP_HELP_MESSAGE_0003_1, SNP_HELP_MESSAGE_0003_2),
        "quality_check": SNP_HELP_MESSAGE_0004,
        "quality_control": SNP_HELP_MESSAGE_0005,
        "mapping": SNP_HELP_MESSAGE_0006,
        "results": SNP_HELP_MESSAGE_0007,
        "run_back": SNP_HELP_MESSAGE_0008,
    }

    # ERROE
    SNP_ERROR_0001 = "adapter_trimを選択する場合、Adapter(5'), Adapter(3')の内、少なくとも1項目は入力してください。"
    SNP_ERROR_0002 = "variant_callを選択する場合、Mappingを選択にしてください。"
    SNP_ERROR_0003 = "variant_callを選択する場合、vcf、gvcfいずれかを選択にしてください。"
    SNP_ERROR_0004 = "sample_iniを選択してください。"
    SNP_ERROR_0005 = "1つ以上のfastqファイルを選択してください。"
    SNP_ERROR_0006 = "解析結果ファイルが存在しない、またはダウンロードに失敗しました."
    SNP_ERROR_0007 = "指定されたURLが正しくありません。"
    SNP_ERROR_0008 = "正しい拡張子のファイルを指定してください。"
    SNP_ERROR_0009 = "セッションが切れました。操作を最初から行なってください。"
