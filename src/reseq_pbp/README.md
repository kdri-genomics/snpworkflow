# makeconfig.py
`makeconfig.py`は`reseq_pbp`向けのconfigファイルを作成するスクリプトである。基本的にはtmp.confを読み込んで仕様するツールのpathを取得し、そのほかのオプションでreseq_pbp.pyが実施する解析を規定する。

## 使用方法 
```
$ python makeconfig.py
usage: makeconfig.py [-h] [-t TMP] [-oc OC] -g GENOME [-init] -outdir OUTDIR
                     -indir INDIR [-fastqc {T,F}] [-at {T,F}] [-qc {T,F}]
                     [-map {T,F}] [-vc {T,F}] [-a3p A3P] [-a5p A5P] [-aer AER]
                     [-q Q] [-thre THRE] [-log LOG]
$ python makeconfig.py -indir /home/ubuntu/data/data_covid19/sample_data/samples -outdir outdir -g /home/ubuntu/data/test_genome/sequence.fasta.gz -init -fastqc T -qc T -q 30 -at T -a3p ATGCTTAGTG -a5p CAGAAAAACTAA,ACTTTCGATCT -aer 0.1 -map T -vc T -vcf T -gvcf F -t tmp_server.conf
```
| option | required | explanation |
|----|----|----|
| -g, --genome | True | genome fastaへのfull path |
| -indir, --input_dir | True | 入力するfastqが配置してあるディレクトリ |
| -outdir, --output_dir | True | 結果を出力するディレクトリ |
| -t, --tmp_conf | False | 実行するcutadapt, fastqc, bwa, samtools, gatkの実行ファイルを[tools]の中に書いたconfファイルを指定。default : tmp.conf |
| -oc, --output_conf | False | makeconfig.pyが作成するreseq_pbp.pyへの入力となるconfigファイルのファイル名。default : out.conf |
| -fastqc, --fastqc | False | fastqcを実行する(T)か否(F)かの決定をする。default F |
| -at, --adapter_trim | False | adapter trimを実行する(T)か否(F)かの決定をする。default F |
| -qc, --quality_control | False | quality controlを実行する(T)か否(F)かの決定をする。default F |
| -map, --mapping | False | mappingを実行する(T)か否(F)かの決定をする。default F |
| -vc, --variant_call | False | variant_callを実行する(T)か否(F)かの決定をする。default F |
| -a3p, --adapter_3p | False | adapter_trimがTの場合に3p側からtrimする配列。ATTCGG,AATGGCTGのようにカンマ区切りで複数識別。defaultではtrimしない |
| -a5p, --adapter_5p | False | adapter_trimがTの場合に5p側からtrimする配列。ATTCGG,AATGGCTGのようにカンマ区切りで複数識別。defaultではtrimしない |
| -aer, --adapter_error_rate | False | adapter_trimがTの場合、アダプターと配列のエラー許容率。default 0.05 |
| -q, --quality | False | quality_controlがTの場合のqualityのcutoff。default 30 |
| -vcf, --vcf | False | variant_callがTの場合にvcfを出力する(T)か否(F)か。default F |
| -gvcf, --gvcf | False | variant_callがTの場合にgvcfを出力する(T)か否(F)か。default F |
| -thre, --threads | False | mapping時に仕様するCPU数。default 1 |
| -log, --log | False | makeconfig.pyのログ。default makeconfig_log.txt |
| -init, --initialization | False | -genomeで入力したゲノムにreseq_pbp.pyを実行するために必要がインデックスが揃っているか確認し、なければ作成するオプション。default False |


### tmp.confの例
Dockerfileで構築されるコンテナ環境内
```
[tools]
fastqc = /home/tools/FastQC/fastqc
cutadapt = /usr/local/bin/cutadapt
bwa = /home/tools/bwa-0.7.17/bwa
samtools = /home/tools/samtools-1.12/samtools
gatk = /home/tools/gatk-4.2.0.0/gatk
```
ancat_x86_64内
```
[tools]
fastqc = /usr/bin/fastqc
cutadapt = /home/ubuntu/.local/bin/cutadapt
bwa = /usr/bin/bwa
samtools = /usr/bin/samtools
gatk = /usr/local/bin/gatk
```

### out.confの例
ancat_x86_64上
```
# command : makeconfig.py -indir /home/ubuntu/data/data_covid19/sample_data/samples -outdir outdir -g /home/ubuntu/data/test_genome/sequence.fasta.gz -init -fastqc T -qc T -q 30 -at T -a3p ATGCTTAGTG -a5p CAGAAAAACTAA,ACTTTCGATCT -aer 0.1 -map T -vc T -vcf T -gvcf F -t tmp_server.conf
[tools]
fastqc = /usr/bin/fastqc
cutadapt = /home/ubuntu/.local/bin/cutadapt
bwa = /usr/bin/bwa
samtools = /usr/bin/samtools
gatk = /usr/local/bin/gatk
[settings]
genome_index = /home/ubuntu/data/test_genome/sequence.fasta.gz
input_dir = /home/ubuntu/data/data_covid19/sample_data/samples
output_dir = outdir
fastqc = T
adapter_trim = T
quality_control = T
mapping = T
variant_call = T
markduplicates = T
emit_vcf = T
emit_gvcf = F
threads = 1
quality_cutoff = 30
adapters_3p = ATGCTTAGTG
adapters_5p = CAGAAAAACTAA,ACTTTCGATCT
adapter_error_rate = 0.1
```

### ヘルプ表示
```
$ python makeconfig.py --help
usage: makeconfig.py [-h] [-t TMP] [-oc OC] -g GENOME [-init] -outdir OUTDIR
                     -indir INDIR [-fastqc {T,F}] [-at {T,F}] [-qc {T,F}]
                     [-map {T,F}] [-vc {T,F}] [-vcf {T,F}] [-gvcf {T,F}]
                     [-a3p A3P] [-a5p A5P] [-aer AER] [-q Q] [-thre THRE]
                     [-log LOG]

reseq config maker

optional arguments:
  -h, --help            show this help message and exit
  -t TMP, --tmp_conf TMP
                        template config file to get path to tools
  -oc OC, --output_conf OC
                        output config file
  -g GENOME, --genome GENOME
                        path to genome fasta
  -init, --initialization
                        check and make index of genome for reseq execution
  -outdir OUTDIR, --output_dir OUTDIR
                        directory to output results
  -indir INDIR, --input_dir INDIR
                        directory which contains input fastqs
  -fastqc {T,F}, --fastqc {T,F}
                        choose from T(rue) and F(alse)
  -at {T,F}, --adapter_trim {T,F}
                        choose from T(rue) and F(alse)
  -qc {T,F}, --quality_control {T,F}
                        choose from T(rue) and F(alse)
  -map {T,F}, --mapping {T,F}
                        choose from T(rue) and F(alse)
  -vc {T,F}, --variant_call {T,F}
                        choose from T(rue) and F(alse)
  -vcf {T,F}, --emit_vcf {T,F}
                        choose from T(rue) and F(alse)
  -gvcf {T,F}, --emit_gvcf {T,F}
                        choose from T(rue) and F(alse)
  -a3p A3P, --adapter_3p A3P
                        3' adapters : -a3p ATCGGA,AAAGGGCCC
  -a5p A5P, --adapter_5p A5P
                        5' adapters : -a5p ATCGGA,AAAGGGCCC
  -aer AER, --adapter_error_rate AER
                        adapter error rate : default 0.05
  -q Q, --quality Q     base cutoff quality : default 30
  -thre THRE, --threads THRE
                        num of threads : default 1
  -log LOG, --log LOG   makeconfig logfile : default makeconfig_log.txt
```

# reseq_pbp.py
reseqをpart by partで実行できるようにしたパイプライン。fastqcでのquality check, cutadaptでのadapter trim / quality trim, bwaでのmapping, gatkでのvariant callからなる。それぞれの実行を制御するconfファイルはmakeconfig.pyを用いて作成する。
## 使用方法
```
$ python reseq_pbp.py --help
usage: reseq_pbp.py [-h] -c CONF -i INI
$ python reseq_pbp.py -c out.conf -i sample.ini
```

| option | required | explanation |
|----|----|----|
| -c, --conf | True | makeconfig.pyで作成した、実行するプロセスなどを設定したconfファイル |
| -i, ini | True | 検体名と入力ファイルを対応付けたCSVファイル |

### sample.iniの例
アップロードしたファイル名と検体名を以下の書き方で対応づける。
```
[sample_name],[fastq read1],[fastq read2]
...
```
ペア（sampleA）、シングルエンド（sampleB）それぞれをアップした場合
```
sampleA,AAA_R1.fq.gz,AAA_R2.fq.gz
sampleB,BBB_R1.fastq
```
sorted bamをアップして変異のcallだけやり直す場合
```
sampleA,sampleA_sorted.bam
sampleB,sampleB_sorted.bam
```

### ヘルプ
```
$ python reseq_pbp.py --help
usage: reseq_pbp.py [-h] -c CONF -i INI
reseq input information

optional arguments:
  -h, --help            show this help message and exit
  -c CONF, --conf CONF  config file
  -i INI, --ini INI     sample.ini file
```

### 出力ファイル
execute on docker container / output filesで記載

## execute on docker container
test on ancat_x86_64
```
$ docker image build -f Dockerfile -t reseq_pbp .
$ docker run -itd -v /home/ubuntu/data:/home/data --name pbp reseq_pbp /bin/bash
$ docker exec -it pbp /bin/bash
# python makeconfig.py -indir /home/data/data_covid19/sample_data/samples -outdir outdir -g /home/data/test_genome/sequence.fasta.gz -init -fastqc T -qc T -q 30 -at T -a3p ATGCTTAGTG -a5p CAGAAAAACTAA,ACTTTCGATCT -aer 0.1 -map T -vc T -vcf T -gvcf T -oc test.conf
# python reseq_pbp.py -i sample.ini -c test.conf 
```

### input files

#### sample.ini
存在しないファイルやファイル自体の記載がない行は無視される
```
sampleA,SRR15224719_1.fastq,SRR15224719_2.fastq
samps,aa
sa
sampleB,SRR15246795_1.fastq
```

#### test.conf
```
# command : makeconfig.py -indir /home/data/data_covid19/sample_data/samples -outdir outdir -g /home/data/test_genome/sequence.fasta.gz -init -fastqc T -qc T -q 30 -at T -a3p ATGCTTAGTG -a5p CAGAAAAACTAA,ACTTTCGATCT -aer 0.1 -map T -vc T -vcf T -gvcf T -oc test.conf
[tools]
fastqc = /home/tools/FastQC/fastqc
cutadapt = /usr/local/bin/cutadapt
bwa = /home/tools/bwa-0.7.17/bwa
samtools = /home/tools/samtools-1.12/samtools
gatk = /home/tools/gatk-4.2.0.0/gatk
[settings]
genome_index = /home/data/test_genome/sequence.fasta.gz
input_dir = /home/data/data_covid19/sample_data/samples
output_dir = outdir
fastqc = T
adapter_trim = T
quality_control = T
mapping = T
variant_call = T
markduplicates = T
emit_vcf = T
emit_gvcf = T
threads = 1
quality_cutoff = 30
adapters_3p = ATGCTTAGTG
adapters_5p = CAGAAAAACTAA,ACTTTCGATCT
adapter_error_rate = 0.1
```

### output files
```
outdir
|-- adapter_trim
|   |-- sampleA_adapter_trim_R1.fq.gz
|   |-- sampleA_adapter_trim_R2.fq.gz
|   `-- sampleB_adapter_trim_R1.fq.gz
|-- fastqc
|   |-- SRR15224719_1_fastqc.html
|   |-- SRR15224719_1_fastqc.zip
|   |-- SRR15224719_2_fastqc.html
|   |-- SRR15224719_2_fastqc.zip
|   |-- SRR15246795_1_fastqc.html
|   `-- SRR15246795_1_fastqc.zip
|-- log.txt
|-- mapping
|   |-- sampleA.bam
|   |-- sampleA_sorted.bam
|   |-- sampleA_sorted.bam.bai
|   |-- sampleB.bam
|   |-- sampleB_sorted.bam
|   `-- sampleB_sorted.bam.bai
|-- quality_control
|   |-- sampleA_quality_control_R1.fq.gz
|   |-- sampleA_quality_control_R2.fq.gz
|   `-- sampleB_quality_control_R1.fq.gz
|-- sample.ini
|-- test.conf
`-- variant_call
    |-- sampleA.g.vcf.gz
    |-- sampleA.g.vcf.gz.tbi
    |-- sampleA.vcf.gz
    |-- sampleA.vcf.gz.tbi
    |-- sampleA_markdup.bam
    |-- sampleA_metrics.txt
    |-- sampleA_plus_RG.bam
    |-- sampleA_plus_RG.bam.bai
    |-- sampleB.g.vcf.gz
    |-- sampleB.g.vcf.gz.tbi
    |-- sampleB.vcf.gz
    |-- sampleB.vcf.gz.tbi
    |-- sampleB_markdup.bam
    |-- sampleB_metrics.txt
    |-- sampleB_plus_RG.bam
    `-- sampleB_plus_RG.bam.bai
```

| dir / file | explanation |
| ---- | ---- |
| file : log.txt | reseq_pbp.pyのログファイル |
| file : sample.ini | 入力したini |
| file : test.conf | 入力したconf |
| dir : fastqc | fastqcの結果を格納するディレクトリ。|
| file : fastqc/*.html | 各サンプルのfastqcのレポート。|
| file : fastqc/*.zip | 各サンプルの図などをまとめたzip。|
| dir : adapter_trim | adapter切除後のfastqを格納するディレクトリ。|
| file : adapter_trim/*.fq.gz | adapter切除後の各サンプルのforward / reverseそれぞれのfastq。|
| dir : quality_control | quality control後のfastqを格納するディレクトリ。|
| file : quality_control/*.fq.gz | quality control後の各サンプルのforward / reverseそれぞれのfastq。|
| dir : mapping | mapping結果を格納するディレクトリ。|
| file : mapping/*.bam | 各サンプルのmapping結果。|
| file : mapping/*_sorted.bam | 各サンプルのsort済みのmapping結果。|
| file : mapping/*.bam.bai | bamのIndex。|
| dir : variant_call | variant callの結果を格納するディレクトリ。|
| file : variant_call/*_markdup.bam | 重複するリードをマークしたbam。|
| file : variant_call/*_metrics.txt | 重複するリードをマークした際のまとめ。|
| file : variant_call/*_plus_RG.bam | 重複するリードをマークしたbamにRead Groupを加えて変異検出に向けて準備したもの。|
| file : variant_call/*.bam.bai | bamのIndex。|
| file : variant_call/*.vcf.gz | 変異検出結果のvcf。|
| file : variant_call/*.vcf.gz.tbi | 変異検出結果のvcfのindex。|
| file : variant_call/*.g.vcf.gz | 変異検出結果のgvcf。|
| file : variant_call/*.g.vcf.gz.tbi | 変異検出結果のgvcfのindex。|


