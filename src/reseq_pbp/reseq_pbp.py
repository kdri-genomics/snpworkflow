import argparse
import configparser
import logging
import os
import subprocess
import sys
from subprocess import PIPE


class Resequencing:
    def __init__(self, ini="sample.ini", conf="config.conf"):
        self.ini = ini
        self.conf = conf

    def setup_logger(self, name=None):
        os.makedirs(self.settings["output_dir"], exist_ok=True)
        logger = logging.getLogger(name)
        logger.setLevel(logging.DEBUG)
        # creates a file handler that logs messages above DEBUG level
        fh = logging.FileHandler(self.logfile)
        fh.setLevel(logging.DEBUG)
        fh_formatter = logging.Formatter(
            "[%(asctime)s] %(levelname)s %(filename)s %(funcName)s :\n%(message)s"
        )
        fh.setFormatter(fh_formatter)
        # creates a file handler that logs messages above INFO level
        sh = logging.StreamHandler()
        sh.setLevel(logging.INFO)
        sh_formatter = logging.Formatter(
            "[%(asctime)s] %(levelname)s %(filename)s : %(message)s",
            "%Y-%m-%d %H:%M:%S",
        )
        sh.setFormatter(sh_formatter)
        # add the handlers to logger
        logger.addHandler(fh)
        logger.addHandler(sh)
        self.logger = logger

    def execute_cmd(self, cmd, shell=True):
        self.logger.info("[cmd] {}".format(cmd))
        proc = subprocess.run(cmd, shell=shell, stdout=PIPE, stderr=PIPE, text=True)
        stdout = proc.stdout
        stderr = proc.stderr
        if len(stdout) > 1:
            self.logger.info(stdout)
        if len(stderr) > 1:
            self.logger.info(stderr)
        return stdout, stderr

    def copy_conf_and_ini(self):
        copy_cmd = "cp {sampleini} {outdir} && cp {conf} {outdir}"
        cmd = copy_cmd.format(
            sampleini=self.ini, outdir=self.settings["output_dir"], conf=self.conf
        )
        self.execute_cmd(cmd)

    def parse_config(self):
        conf = configparser.ConfigParser()
        conf.optionxform = str
        conf.read(self.conf, "UTF-8")
        self.tools = dict(conf.items("tools"))
        self.settings = dict(conf.items("settings"))
        self.logfile = os.path.join(self.settings["output_dir"], "log.txt")

    def parse_ini(self):
        with open(self.ini, "r") as ini:
            lines = ini.readlines()
        self.files_to_process = {}
        for line in lines:
            data = [i.strip() for i in line.rstrip().split(sep=",")]
            lens = len(data)
            if lens > 3 or lens < 2:
                self.logger.info("skip line : {}".format(line))
                continue
            fl = [os.path.join(self.settings["input_dir"], i) for i in data[1:]]
            # check input file path
            for infq in fl:
                ToF = os.path.isfile(infq)
                if ToF is False:
                    break
            if ToF is False:
                self.logger.info(
                    "{infq} no such fastq : skip sample : {sample}".format(
                        infq=infq, sample=data[0]
                    )
                )
                continue
            # save input files
            self.files_to_process[data[0]] = fl
        self.samples = self.files_to_process.keys()

    def fastqc(self):
        if self.settings["fastqc"] != "T":
            self.logger.info("skip fastqc")
            return 0
        # prepare for fastqc
        outdir = os.path.join(self.settings["output_dir"], "fastqc")
        os.makedirs(outdir, exist_ok=True)
        fastqc = self.tools["fastqc"]
        fastq_files = []
        for sample in self.samples:
            for f in self.files_to_process[sample]:
                fastq_files.append(f)
        cmd_list = [fastqc, "--nogroup -o", outdir] + fastq_files
        cmd = " ".join(cmd_list)
        # execute fastqc
        self.execute_cmd(cmd)

    def adapter_trim(self):
        if self.settings["adapter_trim"] != "T":
            self.logger.info("skip adapter_trim")
            return 0
        outdir = os.path.join(self.settings["output_dir"], "adapter_trim")
        os.makedirs(outdir, exist_ok=True)
        cutadapt = self.tools["cutadapt"]
        cmd4paired_tail = " -o {ofq1} -p {ofq2} {ifq1} {ifq2}"
        cmd4single_tail = " -o {ofq1} {ifq1}"
        # get 3' adapter
        a3p = self.settings["adapters_3p"]
        if len(a3p) > 0:
            adapters_3p = [i.strip() for i in a3p.split(sep=",")]
        else:
            adapters_3p = []
        # get 5' adapter
        a5p = self.settings["adapters_5p"]
        if len(a5p) > 0:
            adapters_5p = [i.strip() for i in a5p.split(sep=",")]
        else:
            adapters_5p = []
        # produce strings cmd
        trim_3p = ""
        for adapter in adapters_3p:
            trim_3p = trim_3p + " -a " + adapter
        trim_5p = ""
        for adapter in adapters_5p:
            trim_5p = trim_5p + " -g " + adapter
        aer = self.settings["adapter_error_rate"]
        if len(aer) > 0:
            error_rate = " -e " + aer
        else:
            error_rate = ""
        cmd4paired = cutadapt + trim_3p + trim_5p + error_rate + cmd4paired_tail
        cmd4single = cutadapt + trim_3p + trim_5p + error_rate + cmd4single_tail
        for sample in self.samples:
            infq_list = self.files_to_process[sample]
            outfq_list = []
            read = 1
            for fastq in infq_list:
                filename = sample + "_adapter_trim_R{}.fq.gz".format(read)
                filepath = os.path.join(outdir, filename)
                outfq_list.append(filepath)
                read = read + 1
            fastq_num = len(infq_list)
            if fastq_num == 2:
                cmd = cmd4paired.format(
                    ofq1=outfq_list[0],
                    ofq2=outfq_list[1],
                    ifq1=infq_list[0],
                    ifq2=infq_list[1],
                )
            elif fastq_num == 1:
                cmd = cmd4single.format(ofq1=outfq_list[0], ifq1=infq_list[0])
            else:
                self.logger.debug("too much files for the sample {}".sample)
                sys.exit(1)
            # execute cmd
            self.execute_cmd(cmd)
            # set next files_to_process
            self.files_to_process[sample] = outfq_list

    def quality_control(self):
        if self.settings["quality_control"] != "T":
            self.logger.info("skip quality_control")
            return 0
        # prepare for quality control
        outdir = os.path.join(self.settings["output_dir"], "quality_control")
        os.makedirs(outdir, exist_ok=True)
        cutadapt = self.tools["cutadapt"]
        cmd4paired = cutadapt + " -q {qual} -o {ofq1} -p {ofq2} {ifq1} {ifq2}"
        cmd4single = cutadapt + " -q {qual} -o {ofq1} {ifq1}"
        qual = self.settings["quality_cutoff"]
        for sample in self.samples:
            infq_list = self.files_to_process[sample]
            outfq_list = []
            read = 1
            for fastq in infq_list:
                filename = sample + "_quality_control_R{}.fq.gz".format(read)
                filepath = os.path.join(outdir, filename)
                outfq_list.append(filepath)
                read = read + 1
            fastq_num = len(infq_list)
            if fastq_num == 2:
                cmd = cmd4paired.format(
                    qual=qual,
                    ofq1=outfq_list[0],
                    ofq2=outfq_list[1],
                    ifq1=infq_list[0],
                    ifq2=infq_list[1],
                )
            elif fastq_num == 1:
                cmd = cmd4single.format(
                    qual=qual, ofq1=outfq_list[0], ifq1=infq_list[0]
                )
            else:
                self.logger.debug("too much files for the sample {}".sample)
                sys.exit(1)
            # execute cmd
            self.execute_cmd(cmd)
            # set next files_to_process
            self.files_to_process[sample] = outfq_list

    def mapping(self):
        if self.settings["mapping"] != "T":
            self.logger.info("skip mapping")
            return 0
        # prepare for mapping
        outdir = os.path.join(self.settings["output_dir"], "mapping")
        os.makedirs(outdir, exist_ok=True)
        bwa = self.tools["bwa"]
        samtools = self.tools["samtools"]
        threads = self.settings["threads"]
        genome = self.settings["genome_index"]
        # mapping and make bam cmd
        cmd4paired_head = bwa + " mem -t {threads} {genome} {ifq1} {ifq2}"
        cmd4single_head = bwa + " mem -t {threads} {genome} {ifq1}"
        cmd_tail = " | " + samtools + " view -@ {threads} -b > {bam}"
        cmd4paired = cmd4paired_head + cmd_tail
        cmd4single = cmd4single_head + cmd_tail
        # sort and make index
        cmd4sort = samtools + " sort -@ {threads} -o {sortedbam} {bam}"
        cmd4ind = samtools + " index {sortedbam}"
        # execute command for each sample
        for sample in self.samples:
            infq_list = self.files_to_process[sample]
            bam = os.path.join(outdir, sample + ".bam")
            sortedbam = os.path.join(outdir, sample + "_sorted.bam")
            fastq_num = len(infq_list)
            if fastq_num == 2:
                cmd = cmd4paired.format(
                    threads=threads,
                    genome=genome,
                    ifq1=infq_list[0],
                    ifq2=infq_list[1],
                    bam=bam,
                )
            elif fastq_num == 1:
                cmd = cmd4single.format(
                    threads=threads, genome=genome, ifq1=infq_list[0], bam=bam
                )
            else:
                self.logger.debug("too much files for the sample {}".sample)
                sys.exit(1)
            # execute mapping cmd
            self.execute_cmd(cmd)
            # execute sort cmd
            cmd = cmd4sort.format(threads=threads, bam=bam, sortedbam=sortedbam)
            self.execute_cmd(cmd)
            # execute index cmd
            cmd = cmd4ind.format(sortedbam=sortedbam)
            self.execute_cmd(cmd)
            # set next files_to_process
            self.files_to_process[sample] = [sortedbam]

    def markduplicates(self):
        if self.settings["markduplicates"] != "T":
            self.logger.info("skip markduplicates")
            return 0
        # prepare for markduplicates
        outdir = os.path.join(self.settings["output_dir"], "variant_call")
        os.makedirs(outdir, exist_ok=True)
        gatk = self.tools["gatk"]
        # mark duplicates
        cmd_mkdp = gatk + " MarkDuplicates -I {inbam} -O {outbam} -M {metrics}"
        for sample in self.samples:
            inbam = self.files_to_process[sample][0]
            outbam = os.path.join(outdir, sample + "_markdup.bam")
            metrics = os.path.join(outdir, sample + "_metrics.txt")
            cmd = cmd_mkdp.format(inbam=inbam, outbam=outbam, metrics=metrics)
            # execute markduplicates
            self.execute_cmd(cmd)
            # set next files_to_process
            self.files_to_process[sample] = [outbam]

    def AddOrReplaceReadGroups(self):
        if self.settings["variant_call"] != "T":
            self.logger.info("skip variant_call")
            return 0
        # prepare for markduplicates
        outdir = os.path.join(self.settings["output_dir"], "variant_call")
        os.makedirs(outdir, exist_ok=True)
        gatk = self.tools["gatk"]
        samtools = self.tools["samtools"]
        # mark duplicates
        cmd_arrg = (
            gatk
            + " AddOrReplaceReadGroups -I {inbam} -O {outbam} --RGLB 1 "
            + "--RGPL platform --RGPU platform_unit --RGSM {sample}"
        )
        cmd4ind = samtools + " index {outbam}"
        for sample in self.samples:
            inbam = self.files_to_process[sample][0]
            outbam = os.path.join(outdir, sample + "_plus_RG.bam")
            cmd = cmd_arrg.format(inbam=inbam, outbam=outbam, sample=sample)
            # execute markduplicates
            self.execute_cmd(cmd)
            # execute index cmd
            cmd = cmd4ind.format(outbam=outbam)
            self.execute_cmd(cmd)
            # set next files_to_process
            self.files_to_process[sample] = [outbam]

    def make_variant_call_cmd(self):
        self.call_cmd = {}
        cmd = "{gatk} HaplotypeCaller -R {genome} -I {inbam} -O {outvcf}"
        if self.settings["emit_vcf"] == "T":
            self.call_cmd["vcf"] = {"cmd": cmd}
        if self.settings["emit_gvcf"] == "T":
            self.call_cmd["g.vcf"] = {"cmd": cmd + " -ERC GVCF"}

    def variant_call(self):
        if self.settings["variant_call"] != "T":
            self.logger.info("skip varinat_call")
            return 0
        # markduplicates
        self.markduplicates()
        # AddOrReplaceReadGroups
        self.AddOrReplaceReadGroups()
        # prepare for variant call
        outdir = os.path.join(self.settings["output_dir"], "variant_call")
        os.makedirs(outdir, exist_ok=True)
        gatk = self.tools["gatk"]
        # threads = self.settings["threads"]
        genome = self.settings["genome_index"]
        # execute variant call
        self.make_variant_call_cmd()
        for sample in self.samples:
            outvcf_list = []
            for vcf_type in self.call_cmd.keys():
                inbam = self.files_to_process[sample][0]
                outvcf = os.path.join(outdir, sample + "." + vcf_type + ".gz")
                cmd = self.call_cmd[vcf_type]["cmd"].format(
                    gatk=gatk, genome=genome, inbam=inbam, outvcf=outvcf
                )
                # variant call
                self.execute_cmd(cmd)
                # append outvcf_list
                outvcf_list.append(outvcf)
            # set next files_to_process
            self.files_to_process[sample] = outvcf_list

    def run(self):
        self.parse_config()
        self.setup_logger()
        self.copy_conf_and_ini()
        self.parse_ini()
        self.fastqc()
        self.adapter_trim()
        self.quality_control()
        self.mapping()
        self.variant_call()


def argument_parser():
    parser = argparse.ArgumentParser(description="reseq input information")
    parser.add_argument(
        "-c", "--conf", dest="conf", type=str, help="config file", required=True
    )
    parser.add_argument(
        "-i", "--ini", dest="ini", type=str, help="sample.ini file", required=True
    )
    return parser.parse_args()


def main():
    args = argument_parser()
    Reseq = Resequencing(ini=args.ini, conf=args.conf)
    Reseq.run()


if __name__ == "__main__":
    main()
