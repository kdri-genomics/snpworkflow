/**
   <!-- safariブラウザだと、location.href()でジャンプしてくれないので<a>を生成してクリックするという手段を使う -->
*/
function k_localeChange2(from,to){
    let toLang = to;
    toLang = toLang.replace(/;/g,"!!semicolon!!");
    toLang = toLang.replace(/!!semicolon!!/g,"&#59;");
    toLang = toLang.replace(/\//g,"&#47;");
    toLang = toLang.replace(/\\/g,"&#92;");
    toLang = toLang.replace(/=/g,"&#61;");
    
    let UA = window.navigator.userAgent;
    let RE =new RegExp('/'+from+'/');
    let path = window.location.pathname;

    if (path.match(RE)){
	path = path.replace('/'+from+'/','/'+toLang+'/');
    }
    else{
	return;
    }
    
    // console.log(UA);
    if(UA.indexOf('Safari') != -1) {
        let a = document.createElement('a');
        a.href = path;
        a.click();
    }
    else{
        location.replace(path);
    }
}
