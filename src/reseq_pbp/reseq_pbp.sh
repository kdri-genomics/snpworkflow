#!/bin/bash

RESEQ_PBP=/home/app/reseq_pbp
INDIR=/home/app/SNPWorkFlow/data/input
OUTDIR=/home/app/SNPWorkFlow/data/output
# RESULTS => genome_name or sample_name
# GENOME=/home/genomes/data_covid19/sequence.fasta.gz
FASTQC=$1
QC=$2
Q_VAL=$3
ADAPTER_TRIM=$4
ADAPTER3=$5
ADAPTER5=$6
MAP=$7
VC=$8
VCF=$9
GVCF=${10}
GENOME=/home/genomes/${11}/${12}
SAMPLE_INI=${13}

if [ "$ADAPTER3" == "None" ];then
    ADAPTER3=""
else
    ADAPTER3="-a3p ${ADAPTER3}"
fi

if [ "$ADAPTER5" == "None" ];then
    ADAPTER5=""
else
    ADAPTER5="-a5p ${ADAPTER5}"
fi

if [ "$Q_VAL" == "None" ];then
    Q_VAL=""
else
    Q_VAL="-q ${Q_VAL}"
fi

cd $RESEQ_PBP
# makeconfig
python makeconfig.py -indir $INDIR -outdir ./outdir -g $GENOME -init -fastqc $FASTQC -qc $QC $Q_VAL -at $ADAPTER_TRIM $ADAPTER3 $ADAPTER5 -aer 0.1 -map $MAP -vc $VC -vcf $VCF -gvcf $GVCF
# reseq_pbp
python reseq_pbp.py -c out.conf -i $SAMPLE_INI

mv makeconfig_log.txt outdir

# 1. 今日の UNIX時刻(=UTCでの経過時間(秒)) を取得
unix_today=$(date +'%s')
# 2. タイムゾーンをズラす(+9:00 を秒に変換している)
unix_today=$((unix_today+32400))
# 3. YYYYMMDD に変換
jst_ymd_today=$(date '+%Y%m%d%H%M' --date "@$unix_today")

RESULTS=outdir-${11}-$jst_ymd_today
mv outdir $RESULTS
zip -r $RESULTS.zip $RESULTS
rm -rf $RESULTS out.conf
# sample.ini
mv $RESULTS.zip $OUTDIR
