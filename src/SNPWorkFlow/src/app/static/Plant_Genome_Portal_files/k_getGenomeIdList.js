/** 読み込んだら直ぐに実行 **/
let kp_genomeAssemblyId =(
    function (){
	let request = new XMLHttpRequest();
	request.open('GET', '/api/1/getGenomeIdList', false);
	request.send(null);
	if (request.status === 200) {return JSON.parse(request.response)}
	else{return {}}
    }
)();
